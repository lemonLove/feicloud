package com.feicloud.common.enums;

/**
 * 数据源
 *
 * @author feicloud
 */
public enum DataSourceType {
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
